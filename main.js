// Get modal element
var modal = document.getElementById('simpleModal');
// Get open modal button
var modalBtn = document.getElementById('modalBtn');
// Gel close button
var closeBtn = document.getElementsByClassName('closeBtn')[0];

//listener for opening modal
modalBtn.addEventListener('click',openModal => {
    modal.style.display = 'block';
});

//listener for closing modal
closeBtn.addEventListener('click', closeModal => {
    modal.style.display = 'none';
});

//Outside click to close
window.addEventListener('click', clickOutside = (e) => {
  if(e.target == modal) {      
    modal.style.display = 'none';
  } 
});
  